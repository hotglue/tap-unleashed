"""Stream type classes for tap-unleashed."""

from singer_sdk import typing as th

from tap_unleashed.client import UnleashedStream


class ProductsStream(UnleashedStream):
    """Define custom stream."""

    name = "product"
    path = "/Products"
    primary_keys = ["Guid"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.Items[*]"

    schema = th.PropertiesList(
        th.Property("ProductCode", th.StringType),
        th.Property("ProductDescription", th.StringType),
        th.Property("Barcode", th.StringType),
        th.Property("PackSize", th.NumberType),
        th.Property("Width", th.NumberType),
        th.Property("Height", th.NumberType),
        th.Property("Depth", th.NumberType),
        th.Property("Weight", th.NumberType),
        th.Property("MinStockAlertLevel", th.NumberType),
        th.Property("MaxStockAlertLevel", th.NumberType),
        th.Property("ReOrderPoint", th.IntegerType),
        th.Property(
            "UnitOfMeasure",
            th.ObjectType(
                th.Property("Guid", th.StringType), th.Property("Name", th.StringType)
            ),
        ),
        th.Property("NeverDiminishing", th.BooleanType),
        th.Property("LastCost", th.NumberType),
        th.Property("DefaultPurchasePrice", th.NumberType),
        th.Property("DefaultSellPrice", th.NumberType),
        th.Property("CustomerSellPrice", th.NumberType),
        th.Property("AverageLandPrice", th.NumberType),
        th.Property("Obsolete", th.BooleanType),
        th.Property("Notes", th.StringType),
        th.Property(
            "Images",
            th.ArrayType(
                th.ObjectType(
                    th.Property("Url", th.StringType),
                    th.Property("IsDefault", th.BooleanType),
                )
            ),
        ),
        th.Property("ImageUrl", th.StringType),
        th.Property(
            "SellPriceTier1",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier2",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier3",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier4",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier5",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier6",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier7",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier8",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier9",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property(
            "SellPriceTier10",
            th.ObjectType(
                th.Property("Name", th.StringType),
                th.Property("Value", th.StringType),
            ),
        ),
        th.Property("XeroTaxCode", th.StringType),
        th.Property("XeroTaxRate", th.NumberType),
        th.Property("TaxablePurchase", th.BooleanType),
        th.Property("TaxableSales", th.BooleanType),
        th.Property("XeroSalesTaxCode", th.StringType),
        th.Property("XeroSalesTaxRate", th.NumberType),
        th.Property("IsComponent", th.BooleanType),
        th.Property("IsAssembledProduct", th.BooleanType),
        th.Property(
            "ProductGroup",
            th.ObjectType(
                th.Property("GroupName", th.StringType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("XeroSalesAccount", th.StringType),
        th.Property("XeroCostOfGoodsAccount", th.StringType),
        th.Property("PurchaseAccount", th.StringType),
        th.Property("BinLocation", th.StringType),
        th.Property(
            "Supplier",
            th.ObjectType(
                th.Property("Guid", th.StringType),
                th.Property("SupplierCode", th.StringType),
                th.Property("SupplierName", th.StringType),
                th.Property("SupplierProductCode", th.StringType),
                th.Property("SupplierProductDescription", th.StringType),
                th.Property("SupplierProductPrice", th.NumberType),
            ),
        ),
        th.Property("AttributeSet", th.StringType),
        th.Property("SourceId", th.StringType),
        th.Property("SourceVariantParentId", th.StringType),
        th.Property("IsSerialized", th.BooleanType),
        th.Property("IsBatchTracked", th.BooleanType),
        th.Property("IsSellable", th.BooleanType),
        th.Property("MinimumSellPrice", th.NumberType),
        th.Property("MinimumSaleQuantity", th.NumberType),
        th.Property("CreatedBy", th.StringType),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("LastModifiedBy", th.StringType),
        th.Property("CommerceCode", th.StringType),
        th.Property("CustomsDescription", th.StringType),
        th.Property("SupplementaryClassificationAbbreviation", th.StringType),
        th.Property("ICCCountryCode", th.StringType),
        th.Property("ICCCountryCode", th.StringType),
        th.Property(
            "InventoryDetails",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "Warehouse",
                        th.ObjectType(
                            th.Property("Guid", th.StringType),
                            th.Property("WarehouseCode", th.StringType),
                            th.Property("WarehouseName", th.StringType),
                        ),
                    ),
                    th.Property("WarehouseMinStockAlertLevel", th.NumberType),
                    th.Property("WarehouseMaxStockAlertLevel", th.NumberType),
                )
            ),
        ),
        th.Property("Guid", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
    ).to_dict()


class SalesOrdersStream(UnleashedStream):
    """Define custom stream."""

    name = "sales_order"
    path = "/SalesOrders"
    primary_keys = ["Guid"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.Items[*]"

    schema = th.PropertiesList(
        th.Property(
            "SalesOrderLines",
            th.ArrayType(
                th.ObjectType(
                    th.Property("LineNumber", th.IntegerType),
                    th.Property("LineType", th.StringType),
                    th.Property(
                        "Product",
                        th.ObjectType(
                            th.Property("Guid", th.StringType),
                            th.Property("ProductCode", th.StringType),
                            th.Property("ProductDescription", th.StringType),
                        ),
                    ),
                    th.Property("DueDate", th.DateTimeType),
                    th.Property("OrderQuantity", th.NumberType),
                    th.Property("UnitPrice", th.NumberType),
                    th.Property("DiscountRate", th.NumberType),
                    th.Property("LineTotal", th.NumberType),
                    th.Property("Volume", th.NumberType),
                    th.Property("Weight", th.NumberType),
                    th.Property("Comments", th.StringType),
                    th.Property("AverageLandedPriceAtTimeOfSale", th.NumberType),
                    th.Property("TaxRate", th.NumberType),
                    th.Property("LineTax", th.NumberType),
                    th.Property("XeroTaxCode", th.StringType),
                    th.Property("BCUnitPrice", th.NumberType),
                    th.Property("BCLineTotal", th.NumberType),
                    th.Property("BCLineTax", th.NumberType),
                    th.Property("LineTaxCode", th.StringType),
                    th.Property("XeroSalesAccount", th.StringType),
                    th.Property("SerialNumbers", th.StringType),
                    th.Property("BatchNumbers", th.StringType),
                    th.Property("Guid", th.StringType),
                    th.Property("LastModifiedOn", th.DateTimeType),
                )
            ),
        ),
        th.Property("OrderNumber", th.StringType),
        th.Property("OrderDate", th.DateTimeType),
        th.Property("RequiredDate", th.DateTimeType),
        th.Property("CompletedDate", th.DateTimeType),
        th.Property("OrderStatus", th.StringType),
        th.Property(
            "Customer",
            th.ObjectType(
                th.Property("CustomerCode", th.StringType),
                th.Property("CustomerName", th.StringType),
                th.Property("CurrencyId", th.IntegerType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("CustomerRef", th.StringType),
        th.Property("Comments", th.StringType),
        th.Property(
            "Warehouse",
            th.ObjectType(
                th.Property("WarehouseCode", th.StringType),
                th.Property("WarehouseName", th.StringType),
                th.Property("IsDefault", th.BooleanType),
                th.Property("StreetNo", th.StringType),
                th.Property("AddressLine1", th.StringType),
                th.Property("AddressLine2", th.StringType),
                th.Property("Suburb", th.StringType),
                th.Property("City", th.StringType),
                th.Property("Region", th.StringType),
                th.Property("Country", th.StringType),
                th.Property("PostCode", th.StringType),
                th.Property("PhoneNumber", th.StringType),
                th.Property("FaxNumber", th.StringType),
                th.Property("MobileNumber", th.StringType),
                th.Property("DDINumber", th.StringType),
                th.Property("ContactName", th.StringType),
                th.Property("Obsolete", th.BooleanType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("ReceivedDate", th.DateTimeType),
        th.Property("DeliveryContact", th.ObjectType(
            th.Property("FirstName", th.StringType),
            th.Property("Guid", th.StringType),
            th.Property("LastName", th.StringType),
        )),
        th.Property("DeliveryInstruction", th.StringType),
        th.Property("DeliveryName", th.StringType),
        th.Property("DeliveryStreetAddress", th.StringType),
        th.Property("DeliveryStreetAddress2", th.StringType),
        th.Property("DeliverySuburb", th.StringType),
        th.Property("DeliveryCity", th.StringType),
        th.Property("DeliveryRegion", th.StringType),
        th.Property("DeliveryCountry", th.StringType),
        th.Property("DeliveryPostCode", th.StringType),
        th.Property(
            "Currency",
            th.ObjectType(
                th.Property("CurrencyCode", th.StringType),
                th.Property("Description", th.StringType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("ExchangeRate", th.NumberType),
        th.Property("DiscountRate", th.NumberType),
        th.Property(
            "Tax",
            th.ObjectType(
                th.Property("TaxCode", th.StringType),
                th.Property("Description", th.StringType),
                th.Property("TaxRate", th.NumberType),
                th.Property("CanApplyToExpenses", th.BooleanType),
                th.Property("CanApplyToRevenue", th.BooleanType),
                th.Property("Obsolete", th.BooleanType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("XeroTaxCode", th.StringType),
        th.Property("TaxRate", th.NumberType),
        th.Property("SubTotal", th.NumberType),
        th.Property("TaxTotal", th.NumberType),
        th.Property("Total", th.NumberType),
        th.Property("TotalVolume", th.NumberType),
        th.Property("TotalWeight", th.NumberType),
        th.Property("BCSubTotal", th.NumberType),
        th.Property("BCTaxTotal", th.NumberType),
        th.Property("BCTotal", th.NumberType),
        th.Property("PaymentDueDate", th.DateTimeType),
        th.Property("AllocateProduct", th.BooleanType),
        th.Property("SalesOrderGroup", th.StringType),
        th.Property("DeliveryMethod", th.StringType),
        th.Property(
            "SalesPerson",
            th.ObjectType(
                th.Property("Email", th.StringType),
                th.Property("FullName", th.StringType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
                th.Property("Obsolete", th.BooleanType),
            ),
        ),
        th.Property("SendAccountingJournalOnly", th.BooleanType),
        th.Property("SourceId", th.StringType),
        th.Property("CreatedBy", th.StringType),
        th.Property("CustomOrderStatus", th.StringType),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("LastModifiedBy", th.StringType),
        th.Property("Guid", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
    ).to_dict()


class StockOnHandStream(UnleashedStream):
    """Define custom stream."""

    name = "stock_on_hand"
    path = "/StockOnHand"
    primary_keys = ["Guid"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.Items[*]"

    schema = th.PropertiesList(
        th.Property("ProductCode", th.StringType),
        th.Property("ProductDescription", th.StringType),
        th.Property("ProductGuid", th.StringType),
        th.Property("ProductSourceId", th.StringType),
        th.Property("ProductGroupName", th.StringType),
        th.Property("WarehouseId", th.StringType),
        th.Property("Warehouse", th.StringType),
        th.Property("WarehouseCode", th.StringType),
        th.Property("DaysSinceLastSale", th.IntegerType),
        th.Property("OnPurchase", th.NumberType),
        th.Property("AllocatedQty", th.NumberType),
        th.Property("AvailableQty", th.NumberType),
        th.Property("QtyOnHand", th.NumberType),
        th.Property("AvgCost", th.NumberType),
        th.Property("TotalCost", th.NumberType),
        th.Property("Guid", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
    ).to_dict()


class SuppliersStream(UnleashedStream):
    """Define custom stream."""

    name = "supplier"
    path = "/Suppliers"
    primary_keys = ["Guid"]
    replication_key = None
    records_jsonpath = "$.Items[*]"

    schema = th.PropertiesList(
        th.Property("SupplierCode", th.StringType),
        th.Property("SupplierName", th.StringType),
        th.Property("Guid", th.StringType),
        th.Property(
            "Currency",
            th.ObjectType(
                th.Property("CurrencyCode", th.StringType),
                th.Property("Description", th.StringType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CreatedBy", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
    ).to_dict()


class PurchaseOrdersStream(UnleashedStream):
    """Define custom stream."""

    name = "purchase_orders"
    path = "/PurchaseOrders"
    primary_keys = ["Guid"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.Items[*]"

    schema = th.PropertiesList(
        th.Property("OrderNumber", th.StringType),
        th.Property("OrderDate", th.DateTimeType),
        th.Property("RequiredDate", th.DateTimeType),
        th.Property("DeliveryDate", th.DateTimeType),
        th.Property("CompletedDate", th.DateTimeType),
        th.Property(
            "Supplier",
            th.ObjectType(
                th.Property("Guid", th.StringType),
                th.Property("SupplierCode", th.StringType),
                th.Property("SupplierName", th.StringType),
            ),
        ),
        th.Property(
            "Currency",
            th.ObjectType(
                th.Property("CurrencyCode", th.StringType),
                th.Property("Description", th.StringType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("SupplierRef", th.StringType),
        th.Property("Comments", th.StringType),
        th.Property("Printed", th.StringType),
        th.Property("OrderStatus", th.StringType),
        th.Property("ReceivedDate", th.DateTimeType),
        th.Property("DeliveryName", th.StringType),
        th.Property("DeliveryStreetAddress", th.StringType),
        th.Property("DeliveryStreetAddress2", th.StringType),
        th.Property("DeliverySuburb", th.StringType),
        th.Property("DeliveryRegion", th.StringType),
        th.Property("DeliveryCity", th.StringType),
        th.Property("DeliveryCountry", th.StringType),
        th.Property("ExchangeRate", th.NumberType),
        th.Property("TaxRate", th.NumberType),
        th.Property("XeroTaxCode", th.StringType),
        th.Property("SubTotal", th.NumberType),
        th.Property("TaxTotal", th.NumberType),
        th.Property("Total", th.NumberType),
        th.Property("TotalVolume", th.NumberType),
        th.Property("TotalWeight", th.NumberType),
        th.Property("SupplierInvoiceDate", th.DateTimeType),
        th.Property("BCSubTotal", th.NumberType),
        th.Property("BCTaxTotal", th.NumberType),
        th.Property("BCTotal", th.NumberType),
        th.Property(
            "PurchaseOrderLines",
            th.ArrayType(
                th.ObjectType(
                    th.Property("Guid", th.StringType),
                    th.Property("LineNumber", th.NumberType),
                    th.Property(
                        "Product",
                        th.ObjectType(
                            th.Property("Guid", th.StringType),
                            th.Property("ProductCode", th.StringType),
                            th.Property("ProductDescription", th.StringType),
                        ),
                    ),
                    th.Property("DueDate", th.DateTimeType),
                    th.Property("DeliveryDate", th.DateTimeType),
                    th.Property("OrderQuantity", th.NumberType),
                    th.Property("UnitPrice", th.NumberType),
                    th.Property("LineTotal", th.NumberType),
                    th.Property("BCUnitPrice", th.NumberType),
                    th.Property("BCSubTotal", th.NumberType),
                    th.Property("LineTax", th.NumberType),
                    th.Property("LastModifiedOn", th.DateTimeType),
                    th.Property("DiscountedUnitPrice", th.NumberType),
                    th.Property("DiscountRate", th.NumberType),
                )
            ),
        ),
        th.Property(
            "Warehouse",
            th.ObjectType(
                th.Property("WarehouseCode", th.StringType),
                th.Property("WarehouseName", th.StringType),
                th.Property("Guid", th.StringType),
                th.Property("LastModifiedOn", th.DateTimeType),
            ),
        ),
        th.Property("DiscountRate", th.NumberType),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("LastModifiedBy", th.StringType),
        th.Property("Guid", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
    ).to_dict()
