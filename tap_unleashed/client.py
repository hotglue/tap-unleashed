"""REST client handling, including hotglueStream base class."""

import base64
import hashlib
import hmac
from typing import Any, Dict, Optional
from datetime import datetime
from pendulum import parse

from memoization import cached

import requests
from singer_sdk.streams import RESTStream


class UnleashedStream(RESTStream):
    """hotglue stream class."""

    url_base = "https://api.unleashedsoftware.com"

    records_jsonpath = "$[*]"
    next_page_token_jsonpath = "$.next_page"

    def unleashed_api_get_request(self, url_base, page_number, url_param):
        # keys
        unleashed_api_id = self.config.get("api-auth-id")
        unleashed_api_key = self.config.get("api-auth-signature")
        # Create url
        signature = hmac.new(
            unleashed_api_key.encode("utf-8"), url_param.encode("utf-8"), hashlib.sha256
        )
        auth_token = signature.digest()
        auth_token64 = base64.b64encode(auth_token)
        headers = {
            "Accept": "application/json",
            "api-auth-id": unleashed_api_id,
            "api-auth-signature": auth_token64,
            "Content-Type": "application/json",
        }
        return headers

    @cached
    def get_starting_time(self, context):
        start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        url_base = "Products/Page"
        unleashed_header = self.unleashed_api_get_request(url_base, 1, "")
        headers.update(unleashed_header)
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if previous_token is None:
            next_page_token = 2
        else:
            next_page_token = previous_token + 1
        res = response.json()
        if res.get("Pagination"):
            if res["Pagination"]["NumberOfPages"] == 1:
                return None
            elif res["Pagination"]["PageNumber"] >= res["Pagination"]["NumberOfPages"]:
                return None
        else:
            return None
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        start_date = self.get_starting_time(context)
        # if self.replication_key and start_date:
        #     params["modifiedSince"] = start_date.strftime("%Y-%m-%d")
        if next_page_token:
            self.path = "/".join(self.path.split("/")[:2]) + "/Page/" + str(next_page_token)
        else:
            self.path = self.path + "/Page/1"
        return params

    def get_datetime_keys(self, schema):
        output = {}
        for key, value in schema["properties"].items():
            if value.get("format") == "date-time":
                output[key] = "date-time"
            elif "array" in value.get("type") and "items" in value and "object" in value["items"]["type"] and value["items"].get("properties"):
                dt_items = self.get_datetime_keys(value["items"])
                if dt_items:
                    output[key] = [dt_items]
            elif "object" in value.get("type") and value.get("properties"):
                dt_items = self.get_datetime_keys(value)
                if dt_items:
                    output[key] = dt_items
        return output

    @property
    @cached
    def datetime_fields(self):
        return self.get_datetime_keys(self.schema)

    def to_datetime(self, datetime_field, row):
        for field in datetime_field:
            if isinstance(row.get(field), dict):
                row[field] = self.to_datetime(datetime_field[field], row[field])
            elif isinstance(row.get(field), list):
                row[field] = [self.to_datetime(datetime_field[field][0], f) for f in row[field]]
            elif row.get(field):
                date_str = row[field]
                timestamp = int(date_str[date_str.find("(")+1:date_str.find(")")])
                row[field] = datetime.utcfromtimestamp(timestamp/1000).replace(tzinfo=None).isoformat()
        return row

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        row = self.to_datetime(self.datetime_fields, row)
        return row