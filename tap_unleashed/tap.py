"""hotglue tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_unleashed.streams import (
    ProductsStream,
    PurchaseOrdersStream,
    SalesOrdersStream,
    StockOnHandStream,
    SuppliersStream,
)

STREAM_TYPES = [
    ProductsStream,
    SalesOrdersStream,
    SuppliersStream,
    StockOnHandStream,
    PurchaseOrdersStream,
]


class TapUnleashed(Tap):
    """Unleashed tap class."""

    name = "tap-unleashed"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "api-auth-id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "api-auth-signature",
            th.StringType,
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapUnleashed.cli()
